//
//  AppDelegate.h
//  Test3
//
//  Created by Vishal Verma on 23/08/18.
//  Copyright © 2018 zendrive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end


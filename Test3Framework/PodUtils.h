//
//  PodUtils.h
//  Test3Framework
//
//  Created by Vishal Verma on 24/08/18.
//  Copyright © 2018 zendrive. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PodUtils : NSObject

- (void)test;

@end

NS_ASSUME_NONNULL_END

Pod::Spec.new do |s|
  s.name         = 'Test3'
  s.version      = '1.0.0'
  s.summary      = 'Test3'

  s.description  = 'Test local repo'

  s.homepage     = 'http://aws.amazon.com/mobile/sdk'
  s.license      = 'Apache License, Version 2.0'
  s.author       = { 'Amazon Web Services' => 'amazonwebservices' }
  s.platform     = :ios, '8.0'
  s.source         = { :git => "https://bitbucket.org/zendrive-root/zendrive_cocoapod.git", :tag => "{s.version}" }
  s.requires_arc = true
  s.source_files = 'Test3Framework/*.{h,m}'
end
